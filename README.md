# What

`ghopac` is _GitHub Organization Pull And Clone_ - a tool to pull/clone lots of Git repositories in parallel with a single command. Supports cloning/pulling all [Github](https://github.com/) organization repositories you have access to, as well as keeping any other cloned repositories up to date regardless of their origin. Either or both features can be used.

# Why

I found myself working at companies with a lot of repositories in their Github organizations, and it was a pain to find much less manually clone all of the ones I cared about. Disk is cheap, so I wrote a [Go](https://golang.org/) program to grab them all at once in parallel and put them into a single top level directory.

# Installation

1. Install [go](https://golang.org/ "Golang")
2. Run `go get gitlab.com/nharward/ghopac`
3. You should have a new executable you can run located at `$GOPATH/bin/ghopac`

# Using

1. Create a Github access token at https://github.com/settings/tokens; minimum access should be reading your orgs and repos
2. Create a config file in `${XDG_CONFIG_HOME}/ghopac/config.json`, example below:
   ```json
   {
       "concurrency": 4,
       "verbose": true,
       "github_access_token": "<from step 1>",
       "orgs": [
           {
               "org": "my_github_org",
               "path": "/my/base/code/dir/for/my_github_org",
               "include_archived": false,
               "skip_patterns": [
                   "^undesired_repo_regex_one",
                   "^undesired_repo_regex_two"
               ]
           }
       ],
       "syncpoints": [
           "/some/other/cloned/repo/dir",
           "/yet/another/separately/cloned/repo/dir"
       ]
   }
   ```
3. Run `ghopac`

# Configuration notes

* This program honors the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) in looking for the location of your configuration file
* You don't have to use Github organizations, you can simply omit `orgs` and just use `syncpoints` (or vice versa)
* `concurrency` defaults to `4` if left unspecified in the configuration file
* `syncpoints` do not have to belong to any particular Github organization; they are just local directories where you expect `git pull` to work correctly
* If you have no configuration file, running the program will fail and output a sample file to get you started

# TODO

* Add command line arguments that override the config file
* Check the issues list for more...
